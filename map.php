  <?php /* Template Name: Mappa */ ?>


  <?php 

    $loop = new WP_Query(array(
      'post_type' => 'impianti',
      'posts_per_page' => -1
    ));

    $mapA = array();

    while ($loop->have_posts()) {
      $loop->the_post(); 

      $pot = (get_field('potenzialita') != '') ? get_field('potenzialita') : '-' ;

      $msg = '<div class=\"map-balloon\"><h2>'.get_the_title().'</h2>'.
      '<span><i class=\"far fa-calendar-alt\"></i>'.get_field('anno').'</span><span><i class=\"fas fa-cubes\"></i>'.get_field('prodotto'). '</span>'.
      '<i class=\"fas fa-users\"></i> '.$pot.' AE</div>';

      $icon = (get_field('avviato_futuro')) ? get_template_directory_uri().'/imgs/marker.active.png' : get_template_directory_uri().'/imgs/marker.future.png';
      
      $ma = array(
        'lat' => floatval(get_field('lat')),
        'lng' => floatval(get_field('long')),
        'info' => $msg,
        'icon' => $icon
      );

      
      array_push($mapA, $ma);
    }
    wp_reset_query();

    $mapJ = json_encode($mapA);
  
  
  ?>

  <?php require_once 'header.php'; ?>



    <div id="map"></div>
    <div class="mapLegend">
      <div>
        <img src="<?php echo get_template_directory_uri().'/imgs/marker.active.png' ?>" alt=""> Attivi
      </div>
      <div>
        <img src="<?php echo get_template_directory_uri().'/imgs/marker.future.png' ?>" alt=""> Futuri
      </div>
    </div>
     
    
    <div data-page="referenze" id="interna" class="container mt-4">
      <div class="row">
        <div class="col-12 text-center">
          <h1>Chi ha scelto Oscar</h1>
        </div>
      </div>
      <div class="row">
        <?php $loop = new WP_Query(array(
          'post_type' => 'clienti',
          'posts_per_page' => -1
        ));
        while ($loop->have_posts()): $loop->the_post(); ?>

        <div class="col-md-3">
          <a href="<?php echo get_field('sito_web'); ?>">
            <div class="customer-box">
              <?php $logo = get_field('logo'); ?>
              <img src="<?php echo $logo['url'];?>" class="img-fluid logo">
              <span class="name"><?php echo get_the_title(); ?></span>
            </div>
          </a>
        </div>
        
      <?php endwhile; wp_reset_query(); ?>
      </div>
    </div>

  
   
    

    <script>
function initMap() {

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 6,
    center: {
      lat: 43.563260000,
      lng: 12.431790000
    }
  });
  var infoWin = new google.maps.InfoWindow();
  // Add some markers to the map.
  // Note: The code uses the JavaScript Array.prototype.map() method to
  // create an array of markers based on a given "locations" array.
  // The map() method here has nothing to do with the Google Maps API.
  var markers = locations.map(function (location, i) {
    var marker = new google.maps.Marker({
      position: location,
      icon: location.icon
      // imagePath: 'https://image.ibb.co/b5hzCK/map_marker.png'
    });

    google.maps.event.addListener(marker, 'click', function (evt) {
      infoWin.setContent(location.info);
      infoWin.open(map, marker);
    })
    return marker;
  });

  //set style options for marker clusters (these are the default styles)
  mcOptions = {
    styles: [
      {
        height: 53,
        url: '<?php echo get_template_directory_uri(); ?>/imgs/marker.cluster.53.png',
        width: 53,
        textColor: 'white'
      },
      {
        height: 56,
        url: '<?php echo get_template_directory_uri(); ?>/imgs/marker.cluster.56.png',
        width: 56,
        textColor: 'white'
      },
      {
        height: 66,
        url: '<?php echo get_template_directory_uri(); ?>/imgs/marker.cluster.66.png',
        width: 66,
        textColor: 'white'
      },
      {
        height: 78,
        url: '<?php echo get_template_directory_uri(); ?>/imgs/marker.cluster.78.png',
        width: 78,
        textColor: 'white'
      },
      {
        height: 90,
        url: '<?php echo get_template_directory_uri(); ?>/imgs/marker.cluster.90.png',
        width: 90,
        textColor: 'white'
      }
    ]
  }

  //init clusterer with your options
  var markerCluster = new MarkerClusterer(map, markers, mcOptions);

  // Add a marker clusterer to manage the markers.
  // var markerCluster = new MarkerClusterer(map, markers, {
  //   // imagePath: '<?php echo get_template_directory_uri(); ?>/imgs/marker.cluster.53.png',
  //   imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
  // });

}




locations = JSON.parse('<?php echo $mapJ ?>');


var legend = document.getElementById('legend');
        for (var key in icons) {
          var type = icons[key];
          var name = type.name;
          var icon = type.icon;
          var div = document.createElement('div');
          div.innerHTML = '<img src="' + icon + '"> ' + name;
          legend.appendChild(div);
        }

map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(legend);

google.maps.event.addDomListener(window, "load", initMap);
    </script>
<?php require_once 'footer.php'; ?>