<?php $page = get_page(get_the_ID()); ?>
<h1><?php echo get_the_title(); ?></h1>
<?php if (has_post_thumbnail()): ?>
  <div id="page_featured">
    <div style="background-image: url(<?php echo get_the_post_thumbnail_url()?>);"></div>
  </div>
<?php endif ?>

<?php $logo = get_field('logo');
if($logo): $logoUrl = $logo['sizes']['medium_large']; ?>
  <div class="product-logo">
    <div style="background-image:url(<?php echo $logoUrl;?>);"></div>
  </div>
<?php endif; ?>

<?php echo apply_filters('the_content', $page->post_content); ?>