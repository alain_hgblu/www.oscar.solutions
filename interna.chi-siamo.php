<?php /* Template Name: Chi Siamo */ ?>

<?php require_once 'header.php'; ?>


<div data-page="chi-siamo" id="interna" class="container">
	<div class="row">
		<!-- <div class="col-md-12 mt-5">
			<?php $page = get_page(get_the_ID()); ?>
			<h1><?php echo get_the_title(); ?></h1>
		</div> -->
	</div>
	<div class="row">
		<?php if (has_post_thumbnail()): ?>
      
		<?php endif; ?>
			<div class="col-12">
				<?php //echo apply_filters('the_content', $page->post_content); ?>
				<?php $layout = get_field('layout'); 
				switch ($layout) {
					case '1':
						require 'interna.layout01.php';
						break;
					
						case '2':
						require 'interna.layout02.php';
						break;

					case '3':
						require 'interna.layout03.php';
						break;
					
					default:
						require 'interna.layout01.php';
						break;
				}?>
			</div>
			
	</div>
	<div class="row justify-content-center">
		<?php

		$args = array(
		    'post_type'      => 'page',
		    'posts_per_page' => -1,
		    'post_parent'    => $post->ID,
		    'order'          => 'ASC'
		 );


		$parent = new WP_Query( $args ); ?>

		<?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
			<div class="col-6 col-md-3 membro">
				<div class="pic"><div class="image" style="background-image: url(<?php echo get_the_post_thumbnail_url()?>);"></div></div>
				<div class="desc text-center">
					<div class="nome"><?php echo get_the_title(); ?></div>
					<div class="professione"><?php echo get_field('professione'); ?></div>
					<div class="email"><a href="mailto:<?php echo get_field('email'); ?>"><?php echo get_field('email'); ?></a></div>
				</div>
			</div>
		<?php endwhile; ?>


	</div>

</div>


























<?php require_once 'footer.php'; ?>