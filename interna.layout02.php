<?php $page = get_page(get_the_ID()); ?>





  
  <div class="row">
    <div class="col-12 mt-5">
      <h1><?php echo get_the_title(); ?></h1>
    </div>
    <?php if (has_post_thumbnail()): ?>
      <div class="col-12">
        <div class="product-featured">
					<div style="background-image: url(<?php echo get_the_post_thumbnail_url()?>); background-position:top;"></div>
					<img src="<?php echo get_the_post_thumbnail_url()?>" alt="" class="img-fluid">
        </div>
			</div>
    <?php endif ?>
  </div>



<div class="row">
  <div class="col-12">
    <h2><?php echo get_field('payoff'); ?></h2>
  </div>
  <?php $content = apply_filters('the_content', $page->post_content); 
  $logo = get_field('logo');
  if($logo) { $logoUrl = $logo['sizes']['medium_large']; }

  $cAfterBlocks = explode('[hg-stop-blocks]', $content); 

  $cA = explode('[hg-change-block]', $cAfterBlocks[0]); 
  $leftText = true;
  $pos = 0;
  $order = 5;
  foreach($cA as $block): 
    $imgClass = ($pos % 2 == 0) ? '' : 'order-md-first' ; ?>

    <?php $image = get_string_between($block, '[hg-block-image]', '[/hg-block-image]');
    $block = str_replace($image, '', $block);
    $block = str_replace('[hg-block-image]', '', $block);
    $block = str_replace('[/hg-block-image]', '', $block);?>

    <div class="col-12 my-md-4 product-box">
      <div class="row d-flex align-items-center">
        <div class="col-md-6">
          <?php echo $block; ?>
        </div>
        <div class="col-md-6 text-center <?php echo $imgClass?>">
          <!-- <?php if($pos == 0): ?>
            <div class="product-logo">
              <div style="background-image:url(<?php echo $logoUrl;?>);"></div>
            </div>
          <?php endif; ?> -->

          <?php if($pos > 0): ?>
            <?php if($image != ''): ?>
              <div class="image-box">
                <?php echo $image; ?>
              </div>
            <?php endif; ?>
          <?php endif; ?>
        </div>
      </div>
    </div>
    

  
    



    <?php $order = $order+2; ?>

    



    <!-- <?php $image = get_string_between($block, '[hg-block-image]', '[/hg-block-image]');
    $block = str_replace($image, '', $block);
    $block = str_replace('[hg-block-image]', '', $block);
    $block = str_replace('[/hg-block-image]', '', $block);?>


    <div class="col-md-6 <?php echo $class;?>">
      <?php echo $block; ?>
    </div>


    <?php if($pos == 0): ?>
      <div class="col-md-6">
        <div class="product-logo">
          <div style="background-image:url(<?php echo $logoUrl;?>);"></div>
        </div>
      </div>
    <?php else: ?>
      <?php if($image != ''): ?>
        <div class="col-md-6">
          <?php echo $image; ?>
        </div>
      <?php endif; ?>
    <?php endif; ?> -->
    

    


  <?php $leftText = !$leftText; $pos++; endforeach; ?>
  <div class="col-12">
    <?php echo $cAfterBlocks[1]; ?>
  </div>
</div>
