  
<?php $page = get_page(get_the_ID()); ?>
<div class="row">
  <div class="col-12 mt-5">
    <h1><?php echo get_the_title(); ?></h1>
  </div>  
  <div class="col-12">
    <div class="product-featured">
      <div style="background-image: url(<?php echo get_the_post_thumbnail_url()?>); background-position:top;">
        </div>
        <img src="<?php echo get_the_post_thumbnail_url()?>" alt="" class="img-fluid">
    </div>
  </div>
  <div class="col-12">
    <?php echo apply_filters('the_content', $page->post_content); ?>
  </div>
</div>


<div class="row">
  <?php $args = array(
      'post_type'      => 'page',
      'posts_per_page' => -1,
      'post_parent'    => $post->ID,
      'order'          => 'ASC'
    );


  $parent = new WP_Query( $args ); ?>

  <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
    
    <div class="col-lg-6  layout03_elem">
      <div class="row">
        <div class="col-md-auto pr-md-0">
          <div class="layout_img">
            <div style="background-image:url(<?php echo get_field('immagine'); ?>)"></div>
          </div>
        </div>
        <div class="col-md">
          <h4><?php echo get_the_title(); ?></h4>
          <?php echo get_field('body'); ?>
        </div>
      </div>
    </div>


    
  <?php endwhile; ?>
</div>