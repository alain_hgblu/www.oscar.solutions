$(function() {
  nav_check();
});

if (window.innerHeight > window.innerWidth) {
  var portrait  = true,
      landscape = false;
} else {
  var portrait  = false,
      landscape = true;
}

$( window ).on( "orientationchange", function( event ) {
  nav_check();
});


/**
 * NAV
 */
function nav_check() {
  console.log('nav_check');

  if (tablet_browser) {
    if (window.matchMedia("(orientation: portrait)").matches) {
      $(".cd-nav-trigger").show();
    }

    if (window.matchMedia("(orientation: landscape)").matches) {
      $(".cd-nav-trigger").hide();
    }
  }

  if (mobile_browser) {
    $(".cd-nav-trigger").show();
  }

  if (!tablet_browser && !mobile_browser) {
    $(".cd-nav-trigger").hide();
  }
}

/**
 * SMOOTH SCROLL
 */

/*if (window.addEventListener) window.addEventListener('DOMMouseScroll', wheel, false);
window.onmousewheel = document.onmousewheel = wheel;

function wheel(event) {
    var delta = 0;
    if (event.wheelDelta) delta = event.wheelDelta / 120;
    else if (event.detail) delta = -event.detail / 3;

    handle(delta);
    if (event.preventDefault) event.preventDefault();
    event.returnValue = false;
}

var goUp = true;
var end = null;
var interval = null;

function handle(delta) {
  var animationInterval = 20; //lower is faster
  var scrollSpeed = 20; //lower is faster

  if (end == null) {
    end = $(window).scrollTop();
  }
  end -= 20 * delta;
  goUp = delta > 0;

  if (interval == null) {
    interval = setInterval(function () {
      var scrollTop = $(window).scrollTop();
      var step = Math.round((end - scrollTop) / scrollSpeed);
      if (scrollTop <= 0 || 
          scrollTop >= $(window).prop("scrollHeight") - $(window).height() ||
          goUp && step > -1 || 
          !goUp && step < 1 ) {
        clearInterval(interval);
        interval = null;
        end = null;
      }
      $(window).scrollTop(scrollTop + step );
    }, animationInterval);
  }
}*/


var webFloat = anime.timeline({
  loop: true,
  autoplay: false
});


webFloat.add({
  targets: '#web_background path',
  translateY: [-5, 0, -5, 0],
  fill: ["#F5F5F5", "#f0f0f0", "#F5F5F5"],
  easing: 'easeInOutCubic',
  duration: 8000,
  delay: function (el, i) {
    return i * 300
  },
  direction: 'normal',
  complete: function (anim) {
    // completedLogEl.value = 'completed : ' + anim.completed;
  }
});


var web = anime.timeline({
  loop: true,
  autoplay: false
});


// * Old animation
// web.add({
//   targets: '#web_graphic path',
//   fill: ["#376f74", "#ace4e4"],
//   scale: 1,
//   easing: 'easeInOutCubic',
//   duration: 1000,
//   delay: function (el, i) {
//     return i * 1000
//   },
//   direction: 'alternate',
//   complete: function (anim) {
//     // completedLogEl.value = 'completed : ' + anim.completed;
//   }
// })
// .add({
//   targets: '#web_graphic path',
//   fill: ["#ace4e4", "#376f74"],
//   scale: 1,
//   easing: 'easeInOutCubic',
//   duration: 500,
//   delay: function (el, i) {
//     return i * 150
//   },
//   direction: 'normal',
//   complete: function (anim) {
//     // completedLogEl.value = 'completed : ' + anim.completed;
//   }
// })
;

// * MAIN WEB ANIMATION
// anime({
//   targets: '#web_graphic path',
//   fill: "#376f74"
// });

// setTimeout(() => {
//   anime({
//     targets: '#web_graphic path',
//     fill: function () {
//       var myArray = ["#376f74", "#ace4e4"];
//       return myArray[Math.floor(Math.random() * myArray.length)];
//     },
//     duration: function () { return anime.random(2000, 3000); },
//     direction: 'alternate',
//     delay: function (el, i) {
//       return i * 500
//     },
//     loop: true
//   });
// }, 500);

//
// ─── NEW APPROACH ───────────────────────────────────────────────────────────────
//
$(document).ready(function () {
  
  var elemArray = [];
  var children = $('#web_graphic svg #light, #web_graphic svg #dark').children();

  $(children).each(function (index, element) {
    $(element).attr("fill", "#376f74");
    
  });

  webAnim(children);
   
});


function webAnim(items) {

  var item = items[Math.floor(Math.random() * items.length)];
  // console.log(item);
  if (window.webAnim.last != item) {
    window.webAnim.last = item;
    var fCurr = $(item).attr("fill");
    if (fCurr == "#376f74") {
      $(item).attr("fill", "#ace4e4");
    } else {
      $(item).attr("fill", "#376f74");
    }
    console.log(fCurr);


    setTimeout(() => {
      webAnim(items);
    }, 1000);
  } else {
    webAnim(items);
    console.log("webAnim: same, skipping...");
  }
}

  





/**
 * MOBILE SUB MENU
 */

$(".has-sub").click(function (e) { 
  console.log("click");
  
    if (!$(this).hasClass("sub-show")) {
      console.log("show");
      e.preventDefault();
      $(this).addClass("sub-show");
      $(".cd-nav-trigger.close-nav").css("z-index", "-1");
    }
  
});

$(".rounded-sub-close").click(function (e) { 
  e.preventDefault();
  console.log("close");
  setTimeout(() => {
    $(this).closest(".rounded-sub").closest(".has-sub").removeClass("sub-show");
    
    if ($(".cd-primary-nav").find(".sub-show").length == 0) {
      $(".cd-nav-trigger.close-nav").css("z-index", "5");
    }

  }, 100);
});



$(".hg-subdropdown .dropdown-toggle").click(function (e) { 
  e.preventDefault();
  e.stopImmediatePropagation();
  e.stopPropagation();
  
  $(this).siblings(".dropdown-menu").toggleClass("show");
});


//
// ─── MENU AUTOCOLLAPSE ──────────────────────────────────────────────────────────
//

var lastScrollTop = 0;
if (!tablet_browser && !mobile_browser || (tablet_browser && landscape)) {

  $(window).scroll(function (event) {
    var st = $(this).scrollTop();

    var nav = $("#main-nav");

    console.log(st);

    if (st > lastScrollTop) {
      
      // nav.css("transform", "translateY(-125px)");
      nav.css({
        "transform": "translateY(-125px)",
        "background": "rgba(255,255,255,0)",
        "box-shadow": "none"
      })

    } else {
      
      nav.css({
        "transform": "translateY(0px)",
        "background": "rgba(255,255,255,1)",
        "box-shadow": "0 3px 6px rgba(0,0,0,0.08), 0 3px 6px rgba(0,0,0,0.12)"
      })

    }

    if (st < 100 || st == 0) {
      nav.css({
        "transform": "translateY(0px)",
        "background": "rgba(255,255,255,0)",
        "box-shadow": "none"
      })
    }
    lastScrollTop = st;
  });
}

//
// ─── PRODUCT FEATURED MOBILE ────────────────────────────────────────────────────
//
