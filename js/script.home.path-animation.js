var $w = $(window),
  $canv = $('#motionPath svg'),
  viewH = $("#viewHeight").height(),
  docH = $(document).height();

$(function() {
	
	if (!mobile_browser && !tablet_browser) {
		ballsWidth();
		ballsHeight();
	}

	if (!mobile_browser && tablet_browser) {
		$("#motionPath").css('transform', 'scale(0.85)');
	}

	


	motionPath_check();

	/*window.addEventListener("orientationchange", function() {
	    alert("the orientation of the device is now " + screen.orientation.angle);
	});

	doOnOrientationChange();*/


	/**
	 * Web-grapic
	 */

	var webW = $('#web_graphic svg').width();
	var winW = $w.width();
	var x = 0.7;

	var xx = (winW*x)/webW;
	    xx = xx.toFixed(2);

	console.log('web '+webW);
	console.log('win '+winW);
	console.log(xx);

	$('#web_graphic svg').css('transform-origin', '100% 0%').css('transform', 'scale('+xx+')');


	/**
	 * Web back
	 */

	var webW = $('#web_background svg').width();
	var winW = $w.width();
	var x = 1;

	var xx = (winW * x) / webW;
	xx = xx.toFixed(2);

	console.log('web ' + webW);
	console.log('win ' + winW);
	console.log(xx);

	$('#web_background svg').css('transform-origin', '100% 0%').css('transform', 'scale(' + xx + ')');

	// for (let i = 0; i < 10; i++) {

	// 	var len = $("#web_background svg path").length;
	// 	var random = Math.floor(Math.random() * len) + 1;
	// 	$("#web_background svg path").eq(random).css("stroke", "yellow");

	// }
	

});


function doOnOrientationChange() {
	console.log('# ORIENTATION');
	console.log(window.orientation);
    switch(window.orientation) {  
      case -90 || 90:
        console.log('landscape');
        motionPath_check();
        break; 
      default:
        console.log('portrait');
        motionPath_check();
        break; 
    }
  console.log('# END ORIENTATION');
}
  
window.addEventListener('orientationchange', doOnOrientationChange);


function motionPath_check() {

	if (!mobile_browser && !tablet_browser) {
		console.log('DESKTOP');
		motionPath();
	}

	if (!mobile_browser && tablet_browser) {
		console.log('TABLET');
		
		if (window.matchMedia("(orientation: portrait)").matches) {
		   console.log('PORTRAIT');
		   $('#motionPath').css('display', 'none');

		}

		if (window.matchMedia("(orientation: landscape)").matches) {
		   console.log('LANDSCAPE');
		   $('#motionPath').css('display', 'block');
		   motionPath();
		}

	}

}


function motionPath() {

	var $w = $(window),
			$canv = $('#motionPath svg'),
			viewH = $("#viewHeight").height(),
			docH = $(document).height();


	var path = anime.path('#motionPath #s1200 path');


	var motionPath = anime({
	  targets: '#motionPath .el',
	  translateX: path('x'),
	  translateY: path('y'),
	  rotate: path('angle'),
	  easing: 'linear',
	  //delay: function(el, i, l) { return i * 100; },
	  autoplay: false
	});


	var offset = 100;
  var tot = docH - viewH;
  tot = $canv.height();
  var curr = ($w.scrollTop() + viewH * 0.4) * 100 / docH;
  curr = ($w.scrollTop() - viewH / 2) * 100 / docH;
  curr = ($w.scrollTop() + offset) / (tot + offset) * 100;
  curr = curr.toFixed(2);
  motionPath.seek(motionPath.duration * (curr / 100));

  console.log('motion path initialized');
	$w.scroll(function() {
		var offset = 100;

	  var tot = docH - viewH;
	  tot = $canv.height();

	  var curr = ($w.scrollTop() + viewH * 0.4) * 100 / docH;
	  curr = ($w.scrollTop() - viewH / 2) * 100 / docH;
	  curr = ($w.scrollTop() + offset) / (tot + offset) * 100;
	  curr = curr.toFixed(2);
	  

	  if (curr <= 100) {
	    //motionPath.seek(motionPath.duration * (curr / 1) / 1);
	    motionPath.seek(motionPath.duration * (curr / 100));
	    //console.log('down');
	  } if (curr > 100) {

	  }

	});

}








function ballsWidth() {

	var contW = $("#balls > .container").width();
	var svgW	= $("#motionPath").width();

	if (contW < svgW) {

		var rapp = contW/svgW;
		//rapp = svgW/contW;

		

		console.log(rapp);
		
		//$("#balls #ballsCont").css('transform', 'scale('+rapp+')').css('padding-top', '56px');
		$("#motionPath").css('transform', 'scale('+(rapp)+')');

	}

	console.log("Widths");
	console.log(contW);
	console.log(svgW);

}



function ballsHeight() {

	var contH = $("#balls > .container").height();
	var svgH	= $("#motionPath").height();

	var diff = svgH - contH;

	var wW = $(window).width();
	console.log("www "+wW);
	var scale = 1;
	var marg = 0.66;
	if (wW > 1500) {
		scale = 1.3;
		marg = 2;
		console.log("balls cont scaled");

	}



	$("#balls > .container").css({
		'transform-origin': 'top',
		'transform': 'scale(' + scale + ')',
		'margin-bottom': diff*marg +'px'
	});

}