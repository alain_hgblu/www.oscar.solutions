<?php require_once 'header.php'; ?>

<?php the_post(); ?>
<div id="blog" class="container" data-page="post">
	<div class="row my-5">
		<div class="col-12">
			<h1><?php the_title(); ?></h1>
		</div>

		<?php if (has_post_thumbnail()): ?><div class="col-12">
			  <div class="feat-img" style="background-image: url(<?php echo get_the_post_thumbnail_url()?>);"></div>
		</div><?php endif; ?>

		<div class="col-4 blog-author">
		  <div class="row row-eq-height">
		    <div class="col-5 pr-0">
		      <?php 
		        $get_author_id = get_the_author_meta('ID');
		        $get_author_gravatar = get_avatar_url($get_author_id, array('size' => 450));

		        echo get_avatar($get_author_id, 156)
		       ?>
		    </div>
		    <div class="col-7">
		      <div class="author-meta">
		        <p class="author-name">
		          <?php echo get_the_author_meta('first_name'); ?>
		          <br>
		          <?php echo get_the_author_meta('last_name'); ?>
		        </p>
		      </div>
		    </div>
		  </div>
		  <div class="row">
		    <div class="col author-meta-social">
		      <a href="<?php echo get_the_author_meta('linkedin'); ?>"><i class="fab fa-linkedin"></i></a>
		      <a href="mailto:<?php echo get_the_author_meta('email'); ?>"><?php echo get_the_author_meta('email'); ?></a>
		    </div>
		  </div>
		</div>
		<div class="col-8 post-cont">
			<?php the_content(); ?>
		</div>
	</div>
</div>

<?php require_once 'footer.php'; ?>