<?php 

	add_theme_support( 'post-thumbnails' );


	/**
	 * USERS
	 */
	add_action( 'show_user_profile', 'extra_user_profile_fields' );
	add_action( 'edit_user_profile', 'extra_user_profile_fields' );

	function extra_user_profile_fields( $user ) { ?>
	    <h3><?php _e("Extra profile information", "blank"); ?></h3>

	    <table class="form-table">
	    <tr>
	        <th><label for="linkedin"><?php _e("LinkedIn"); ?></label></th>
	        <td>
	            <input type="text" name="linkedin" id="address" value="<?php echo esc_attr( get_the_author_meta( 'linkedin', $user->ID ) ); ?>" class="regular-text" /><br />
	            <span class="description"><?php _e("Please enter your address."); ?></span>
	        </td>
	    </tr>
	    </table>
	<?php }

	add_action( 'personal_options_update', 'save_extra_user_profile_fields' );
	add_action( 'edit_user_profile_update', 'save_extra_user_profile_fields' );

	function save_extra_user_profile_fields( $user_id ) {
	    if ( !current_user_can( 'edit_user', $user_id ) ) { 
	        return false; 
	    }
	    update_user_meta( $user_id, 'linkedin', $_POST['linkedin'] );
	}

function create_posttype()
{
	register_post_type(
		'impianti',
		array(
			'labels' => array(
				'name' => __('Impianti'),
				'singular_name' => __('Impianto')
			),
			'public' => true,
			'has_archive' => true,
			'rewrite' => array('slug' => 'impianti'),
			'menu_icon' => 'dashicons-carrot'
		)
	);
}
add_action('init', 'create_posttype');

function create_posttype_customers()
{
	register_post_type(
		'clienti',
		array(
			'labels' => array(
				'name' => __('Clienti'),
				'singular_name' => __('Cliente')
			),
			'public' => true,
			'has_archive' => true,
			'rewrite' => array('slug' => 'clienti'),
			'menu_icon' => 'dashicons-businessman'
		)
	);
}
add_action('init', 'create_posttype_customers');





function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');



function get_string_between($string, $start, $end){
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}