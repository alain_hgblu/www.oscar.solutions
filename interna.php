<?php /* Template Name: Interna */ ?>

<?php require_once 'header.php'; ?>


<div id="interna" class="container">
	<div class="row">
		<div class="col-md-12">

			<?php $layout = get_field('layout'); 
      switch ($layout) {
        case '1':
          require 'interna.layout01.php';
          break;

        case '3':
          require 'interna.layout03.php';
          break;
        
        default:
          require 'interna.layout01.php';
          break;
      }?>
			
		</div>
	</div>
</div>


























<?php require_once 'footer.php'; ?>