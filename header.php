<?php 

	if (function_exists('get_template_directory')) {
		define('HG_PATH', get_template_directory_uri());
		define('HG_HOME', get_home_url());
	} else {
		define('HG_PATH', '.');
		define('HG_HOME', './');
	}

	define('HG_PATH_LOCAL', '.');

	/*function isMobile() {
	    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
	}*/

	$tablet_browser = 0;
	$mobile_browser = 0;
	 
	if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
	    $tablet_browser++;
	}
	 
	if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
	    $mobile_browser++;
	}
	 
	if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
	    $mobile_browser++;
	}
	 
	$mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
	$mobile_agents = array(
	    'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
	    'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
	    'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
	    'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
	    'newt','noki','palm','pana','pant','phil','play','port','prox',
	    'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
	    'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
	    'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
	    'wapr','webc','winw','winw','xda ','xda-');
	 
	if (in_array($mobile_ua,$mobile_agents)) {
	    $mobile_browser++;
	}
	 
	if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'opera mini') > 0) {
	    $mobile_browser++;
	    //Check for tablets on opera mini alternative headers
	    $stock_ua = strtolower(isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA'])?$_SERVER['HTTP_X_OPERAMINI_PHONE_UA']:(isset($_SERVER['HTTP_DEVICE_STOCK_UA'])?$_SERVER['HTTP_DEVICE_STOCK_UA']:''));
	    if (preg_match('/(tablet|ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
	      $tablet_browser++;
	    }
	}

?>


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Oscar Solutions - Your Partner in Process Control</title>

  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="<?php echo HG_PATH; ?>/libs/rounded-animated-navigation/css/reset.css">
  <link href="https://fonts.googleapis.com/css?family=Cairo:200,300,400,600" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo HG_PATH; ?>/node_modules/animate.css/animate.min.css">
  <link rel="stylesheet" href="<?php echo HG_PATH; ?>/libs/hover/hover-min.css">

  <link rel="stylesheet" href="<?php echo HG_PATH; ?>/libs/rounded-animated-navigation/css/style.css">
  <link rel="stylesheet" href="<?php echo HG_PATH; ?>/node_modules/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo HG_PATH; ?>/libs/bootstrap-4-multi-dropdown-navbar/css/bootstrap-4-navbar.css">
  <link rel="stylesheet" href="<?php echo HG_PATH; ?>/css/custom.css">

  <!-- <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script> -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

</head>

<!-- style="background-image: url(<?php echo get_template_directory_uri() ?>/imgs/pattern_molecole.svg);" -->
<body data-home="<?php echo is_home(); ?>">

	

	<nav id="main-nav" class="navbar navbar-expand-lg">
		<div class="container">
		  <a class="navbar-brand mr-auto" href="<?php echo HG_HOME ?>"><img src="<?php echo HG_PATH ?>/imgs/LogoOscar.png" alt=""></a>

		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav ml-auto">
		      <li class="nav-item">
		        <a class="nav-link" href="<?php echo HG_HOME ?>/chi-siamo/">Chi siamo</a>
		      </li>
		      <!-- <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown_perche" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Perché Oscar</a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown_perche">
							<a class="dropdown-item" href="<?php echo HG_HOME ?>/perche-oscar/">Perché Oscar</a>
              <a class="dropdown-item" href="<?php echo HG_HOME ?>/perche-oscar/rispettare-i-limiti-allo-scarico/">Rispettare i limiti allo scarico</a>
              <a class="dropdown-item" href="<?php echo HG_HOME ?>/perche-oscar/risparmio-energetico/">Risparmio energetico</a>
							<a class="dropdown-item" href="<?php echo HG_HOME ?>/perche-oscar/risparmio-di-risorse/">Risparmio di risorse</a>
							<a class="dropdown-item" href="<?php echo HG_HOME ?>/perche-oscar/un-team-al-tuo-servizio/">Un team al tuo servizio</a>
            </div>
          </li> -->
					<li class="nav-item">
		        <a class="nav-link" href="<?php echo HG_HOME ?>/perche-oscar/">Perché Oscar</a>
		      </li>
		      
					<!-- <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown_soluzioni" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Soluzioni</a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown_soluzioni">
              <a class="dropdown-item" href="<?php echo HG_HOME ?>/soluzioni/controllori-di-processo/">Controllori di processo</a>
              <a class="dropdown-item" href="<?php echo HG_HOME ?>/soluzioni/decision-support-system/">Decision Support System</a>
            </div>
          </li> -->

					<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										Soluzioni
								</a>
								<ul class="dropdown-menu hg-subdropdown">
										<li><a class="dropdown-item dropdown-toggle" href="<?php echo HG_HOME ?>">Linea acque</a>
												<ul class="dropdown-menu">
														<li><a class="dropdown-item" href="<?php echo HG_HOME ?>/soluzioni/oscar-zero/">AE < 10 000</a></li>
														<li><a class="dropdown-item" href="<?php echo HG_HOME ?>/soluzioni/oscar-lt/">10 000< AE< 20 000</a></li>
														<li><a class="dropdown-item" href="<?php echo HG_HOME ?>/soluzioni/oscar-std/">AE > 20 000</a></li>
												</ul>
										</li>
										<li><a class="dropdown-item" href="<?php echo HG_HOME ?>/soluzioni/oscar-dewatering/">Linea fanghi</a></li>
										<li><a class="dropdown-item" href="<?php echo HG_HOME ?>/soluzioni/oscar-dashboard/">Decision Support System</a></li>
								</ul>
						</li>
					<li class="nav-item">
		        <a class="nav-link" href="<?php echo HG_HOME ?>/referenze/">Referenze</a>
		      </li>

		      <!-- <li class="nav-item">
		        <a class="nav-link" href="<?php echo HG_HOME ?>/blog/">Blog</a>
		      </li> -->
		      <!-- <li class="nav-item">
		        <a class="nav-link" href="#">Contatti</a>
		      </li> -->
		      <li class="nav-item">
		        <?php if ($tablet_browser): ?>
		        	<a class="nav-link btn btn-oscar ml-3" href="<?php echo HG_HOME ?>/contattaci/">Contattaci</a>
	        	<?php else: ?>
	        		<a class="nav-link btn btn-oscar ml-3" href="<?php echo HG_HOME ?>/contattaci/">Contattaci</a>
		        <?php endif ?>
		      </li>
		  </div>
		</div>
	</nav>

	

	<nav id="mobile-nav">
		<ul class="cd-primary-nav">
			
			<li>
				<a class="" href="<?php echo HG_HOME ?>/chi-siamo/">Chi siamo</a>
			</li>
			<li>
				<a class="" href="<?php echo HG_HOME ?>/perche-oscar/">Perché Oscar</a>
			</li>
			<li class="dropdown">
					<a class="dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Soluzioni
					</a>
					<ul class="dropdown-menu hg-subdropdown">
							<li><a class="dropdown-item dropdown-toggle" href="<?php echo HG_HOME ?>">Linea acque</a>
									<ul class="dropdown-menu">
											<li><a class="dropdown-item" href="<?php echo HG_HOME ?>/soluzioni/oscar-zero/">AE < 10 000</a></li>
											<li><a class="dropdown-item" href="<?php echo HG_HOME ?>/soluzioni/oscar-lt/">10 000< AE< 20 000</a></li>
											<li><a class="dropdown-item" href="<?php echo HG_HOME ?>/soluzioni/oscar-std/">AE > 20 000</a></li>
									</ul>
							</li>
							<li><a class="dropdown-item" href="<?php echo HG_HOME ?>/soluzioni/oscar-dewatering/">Linea fanghi</a></li>
							<li><a class="dropdown-item" href="<?php echo HG_HOME ?>/soluzioni/oscar-dashboard/">Decision Support System</a></li>
					</ul>
			</li>
			<li>
				<a class="" href="<?php echo HG_HOME ?>/referenze/">Referenze</a>
			</li>			
			
			
			<li><a href="<?php echo HG_HOME ?>/contattaci/" class="">Contattaci</a></li>
		</ul>
	</nav>

	<?php $debug = (isset($_GET['deb'])) ? '' : 'display:none;' ; ?>
	<div id="debug" style="<?php echo $debug ?>">
		<div id="what"><div id="a"></div><div id="o"><div id="d"></div></div></div>
	</div>

	<!--<button onclick="detectIPadOrientation();">What's my Orientation?</button>-->



