	<div class="cd-overlay-nav">
		<span></span>
	</div> <!-- cd-overlay-nav -->

	<div class="cd-overlay-content">
		<span></span>
	</div> <!-- cd-overlay-content -->

	<a href="#0" class="cd-nav-trigger">Menu<span class="cd-icon"></span></a>

	<footer class="footer">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<img src="<?php echo HG_PATH; ?>/imgs/LogoOscar_w.png" alt="">
				</div>
				<div class="col-md-4">
					<p>
						<b>E.T.C. Sustainable Solutions S.r.l. </b><br>
						Via Praga 7 - Loc. Spini - 38121 - TRENTO <br>
						T. +39 0461 825966 <br>
						Email: <a href="mailto:info@etc-eng.it">info@etc-eng.it</a> <br>

						PIVA: 02323330221
					</p>
				</div>
				<div class="col-md-4">
					<p>
						<a href="<?php echo HG_HOME ?>/chi-siamo/">Chi siamo</a> /
						<a href="<?php echo HG_HOME ?>/perche-oscar/">Perché Oscar</a> /
						<a href="<?php echo HG_HOME ?>/soluzioni/">Soluzioni</a> /
						<!-- <a href="<?php echo HG_HOME ?>/blog/">Blog</a> / -->
						<a href="<?php echo HG_HOME ?>/contattaci/">Contattaci</a>
						<!-- <a href="<?php echo HG_HOME ?>">Richiesta info</a> / -->
					</p>
					<p>
						Cookie Policy / Privacy /Credits
					</p>
					<!-- <p>
						<button class="btn">Iscriviti alla nostra newsletter</button>
					</p> -->
				</div>
			</div>
		</div>
	</footer>
	

	<script>var tablet_browser = false,
							mobile_browser = false;</script>
	<?php if ($tablet_browser): ?>
	  <script>var tablet_browser = true;</script>
  <?php elseif ($mobile_browser): ?>
  	<script>var mobile_browser = true;</script>
	<?php endif ?>

	



	<script src="<?php echo HG_PATH; ?>/node_modules/jquery/dist/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="<?php echo HG_PATH; ?>/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- <script src="<?php echo HG_PATH; ?>/libs/bootstrap-4-multi-dropdown-navbar/js/bootstrap-4-navbar.js"></script> -->
	<script src="<?php echo HG_PATH; ?>/node_modules/animejs/anime.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
  <script src="<?php echo HG_PATH; ?>/libs/rounded-animated-navigation/js/velocity.min.js"></script>
	<script src="<?php echo HG_PATH; ?>/libs/rounded-animated-navigation/js/main.js"></script> <!-- Resource jQuery -->
	<script>
	new WOW().init();
	</script>
  <script src="<?php echo HG_PATH; ?>/js/script.js"></script>
  <?php if (!function_exists('is_home') or is_home()): ?>
		<script src="<?php echo HG_PATH; ?>/js/script.home.path-animation.js"></script>
	<?php endif ?>
	
	<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
	<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD9A90e5Xg-9uVUk_RNxwveKhV47tnl2_M&callback=initMap">
    </script>

</body>

</html>