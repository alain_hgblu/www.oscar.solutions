<?php $page = get_page(get_the_ID()); ?>

<div id="viewHeight" ></div>

<div id="masterhead">
	<div id="web_graphic">
	  <?php include 'imgs/palline.svg'; ?>
	</div>
	<div id="web_background">
		<?php include 'imgs/molecole_sfondo.svg' ?>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1>
				  Your <b>partner</b><br>
				  in process control
				</h1>
			</div>
		</div>
	</div>
</div>

<div id="balls">
	<div class="container">
		<div id="motionPath" style="transform-origin: top left;">
			<div id="prePath"></div>
			<div class="small square el follow-path"></div>
			<div class="path" id="s1200">
				<?php include 'imgs/_path.svg'; ?>
			</div>
		</div>
		<div id="ballsCont" style="transform-origin: top left;">
			<div class="row ball-l" style="">
			  <div class="col-12 col-sm-6 order-1 order-md-1 pr-md-0">
			    <div class="ball ball-1 mx-auto ml-sm-auto" style="background-image:url('<?php echo HG_PATH; ?>/imgs/ambiente.jpg');">&nbsp;</div>
			  </div>
			  <div class="col-12 col-sm-6 order-2 order-md-2 pl-md-0" style="padding-right:0;">
			    <div class="par">
			      <span>Il rispetto dell'<b>ambiente</b> e la valorizzazione di tutte le sue <b>risorse</b> sono la chiave per lo sviluppo sostenibile.</span>
			    </div>
			  </div>
			</div>
			<div class="row ball-r">
			  <div class="col-12 col-sm-6 order-2 order-md-1 pr-md-0" style="padding-right:0;">
			    <div class="par">
			      <span> L'<b>acqua</b> è il patrimonio ambientale più prezioso e potente strumento con il quale oggi è possibile avviare processi virtuosi di <b>economia circolare</b>.</span>
			    </div>
			  </div>

			  <div class="col-12 col-sm-6 order-1 order-md-2 pl-md-0">
			    <div class="ball ball-2 mx-auto mr-sm-auto" style="background-image:url('<?php echo HG_PATH; ?>/imgs/acqua.jpg');">&nbsp;</div>
			  </div>
			</div>

			<div class="row ball-l">
			  <div class="col-12 col-sm-6 order-1 order-md-1 pr-md-0">
			    <div class="ball ball-3 mx-auto ml-sm-auto" style="background-image:url('<?php echo HG_PATH; ?>/imgs/depurazione.jpg');">&nbsp;</div>
			  </div>
			  <div class="col-12 col-sm-6 order-2 order-md-2 pl-md-0" style="padding-right:0;">
			    <div class="par">
			      <span>Efficienza, innovazione e recupero di risorse nei processi di <b>depurazione</b>: è così che vogliamo costruire il futuro a partire dalle <b>acque reflue</b>.</span>
			    </div>
			  </div>
			</div>
		</div>
	</div>
</div>

<section id="afterBalls">
  <div class="container">
    <div class="row">
      <div class="col">
        <p>
          <b>Nasce per questo Oscar:</b><br>Per dare il nostro contributo nella salvaguardia dell'ambiente e nella valorizzazione delle risorse naturali. <br>Perché che i problemi dei nostri clienti sono anche i nostri.
        </p>
      </div>
    </div>
  </div>
</section>

<section id="valBals" >
  <div class="container">
    <div class="row">
      <div class="col-6 col-lg-3">
        <div class="valBal wow fadeInUp" data-wow-delay="0.7s">
          <span class="num">5<small>mln</small></span>
          <span class="desc">AE serviti</span>
        </div>
      </div>

      <div class="col-6 col-lg-3">
        <div class="valBal wow fadeInUp" data-wow-delay="1.4s">
          <span class="num">80</span>
          <span class="desc">WWTP efficientati</span>
        </div>
      </div>

      <div class="col-6 col-lg-3">
        <div class="valBal wow fadeInUp" data-wow-delay="2.1s">
          <span class="num">30</span>
          <span class="desc">gestori del SII<br>supportati</span>
        </div>
      </div>

      <div class="col-6 col-lg-3">
        <div class="valBal wow fadeInUp" data-wow-delay="2.8s">
          <span class="num">50<small>GWh</small></span>
          <span class="desc">risparmiati ogni anno</span>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="slogan">
  <div class="container">
    <div class="row">
      <div class="col">
        <!-- <h3>"L'acqua è la materia della vita. È matrice, madre e mezzo. Non esiste vita senza acqua."</h3>
        <p>
          di Albert Szent Gyorgyi, premio Nobel per la medicina nel 1937
        </p> -->
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
      </div>
    </div>
  </div>
</section>