<?php /* Template Name: Blog */ ?>
<?php require_once 'header.php'; ?>

  
  <!-- FILTER SNIPPET

    // multiple choices allowed
    // all of this goes inside your foreach loop
    $args = array(
      'post_type' => 'post',
      'posts_per_page' => 1, // only need 1, just checking for any posts with the value
      'meta_query' => array(
        array(
          'key' => 'family_name'
          'value' => '"'.$family.'"',
          'compare => 'LIKE'
        )
      ) 
    )
    $query = new WP_Query($args);
    if (count($query->posts)) {
      echo '<li>' . $family . '</li>';
    ) (### or '}'? ###)

-->


  <div id="blog" class="container">
    <div class="row justify-content-center">

      <div class="col-8">
        <form class="row" action="#">

            <div class="form-group col-3 p-1">
              
              <select name="sett" class="form-control" id="blogSettoreSelect">
                <option value="false">- SETTORE -</option>
                <?php 
                  $field_key = "field_5ac33a4e56ed6";
                  $field = get_field_object($field_key);
                  $families = $field['choices']; ?>
                <?php foreach ($families as $key => $value): ?>
                  <?php $sel = ($_GET['sett'] == $key) ? 'selected' : '' ; ?>
                  <option value="<?php echo $key ?>" <?php echo $sel ?>><?php echo $value ?></option>
                <?php endforeach ?>
              </select>
            </div>
            <div class="form-group col-3 p-1">
              
              <select name="a" class="form-control" id="blogAuthorSelect">
                <option value="false">- AUTORE -</option>
                <?php $users = (array) get_users(); ?>
                <?php foreach ($users as $u): ?>
                  <?php $u = (array) $u; $d = $u['data']; ?>
                  <?php $sel = ($_GET['a'] == $d->user_nicename) ? 'selected' : '' ; ?>
                  <option value="<?php echo $d->user_nicename ?>" <?php echo $sel ?>><?php echo $d->display_name ?></option>
                <?php endforeach ?>
              </select>
            </div><!--
            <div class="form-group col p-1">
              <?php $val = (isset($_GET['q']) && trim($_GET['q']) != '') ? $_GET['q'] : '' ; ?>
              <input name="q" class="form-control" type="text" placeholder="Cerca" value="<?php echo $val ?>">
            </div>-->
            <div class="form-group col p-1">
              <div class="input-group">
                <?php $val = (isset($_GET['q']) && trim($_GET['q']) != '') ? $_GET['q'] : '' ; ?>
                <input name="q" class="form-control" type="text" placeholder="Cerca" value="<?php echo $val ?>">
                <div class="input-group-append" style="margin-left: 0.5rem;">
                  <button type="submit" class="btn btn-secondary"><i class="fas fa-search"></i></button>
                  <?php if ($_GET): ?>
                    <!--<div class="btn btn-secondary" style="border:none;" onclick="window.location = window.location.pathname;">Reset</div>-->
                    <div class="blogSearchReset"><i class="fa fa-times" onclick="window.location = window.location.pathname;"></i></div>
                  <?php endif ?>
                </div>
              </div>
            </div><!--
            <div class="form-group col p-1">
              <button type="submit" class="btn btn-primary" style="background-color: #366F75; border:none;">Vai</button>
              <?php if ($_GET): ?>
                <div class="btn btn-secondary" style="border:none;" onclick="window.location = window.location.pathname;">Reset</div>
              <?php endif ?>
            </div>-->

        </form>
        <script>
          

        </script>
      </div>
    </div>
  	<div class="row">
  
 



  <?php 

    # base 
    $args = array(
        'post_type'      => 'post',
        'orderby'        => 'date',
        'order'          => 'ASC',
        'posts_per_page' => 6,
    );
    # simple search
    if (isset($_GET['q']) && trim($_GET['q']) != '') {
        $args['s'] = trim($_GET['q']);
        $args['posts_per_page'] = 9999;
    }
    #advanced search
    if (isset($_GET['sett']) && trim($_GET['sett']) != '' && trim($_GET['sett']) != 'false') {
        $args['meta_query'] = array(
            'relation' => 'AND',
            array(
                'key' => 'settore',
                'compare' => '=',
                'value' => trim($_GET['sett'])
            )
        );
    }
    if (isset($_GET['a']) && trim($_GET['a']) != '' && trim($_GET['a']) != 'false') {
        $args['author_name'] = $_GET['a'];
    }

    $the_query = new WP_Query($args);


   ?>


  
  <?php 
  while ( $the_query->have_posts() ) : $the_query->the_post();?>

    <article class="col-12 <?php echo get_field('settore'); ?>">
      <div class="row">
        
        <div class="col-4 blog-author">
          <div class="row row-eq-height">
            <div class="col-5 p-0">
              <?php 
                $get_author_id = get_the_author_meta('ID');
                $get_author_gravatar = get_avatar_url($get_author_id, array('size' => 450));

                echo get_avatar($get_author_id, 156)
               ?>
            </div>
            <div class="col-7">
              <div class="author-meta">
                <p class="author-name">
                  <?php echo get_the_author_meta('first_name'); ?>
                  <br>
                  <?php echo get_the_author_meta('last_name'); ?>
                </p>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col author-meta-social">
              <a href="<?php echo get_the_author_meta('linkedin'); ?>"><i class="fab fa-linkedin"></i></a>
              <a href="mailto:<?php echo get_the_author_meta('email'); ?>"><?php echo get_the_author_meta('email'); ?></a>
            </div>
          </div>
        </div>
        <div class="col-8">
          <div class="post-settore">
            <?php 
              $field_name = "settore";
              $field = get_field_object($field_name);
              $value = $field['value'];
             ?>
            <a href="#" class="badge badge-light"><?php echo $field['choices'][$value]; ?></a>
          </div>
          <h3><a class="text-dark" href="<?php echo get_permalink(); ?>"><?php echo get_the_title() ?></a></h3>
          <div class="post tags">
            <?php $t = (array) wp_get_post_tags($post->ID); ?>

            <?php foreach ($t as $tt): ?>
              <a href="#" class="badge badge-light"><?php echo $tt->name ?></a>
            <?php endforeach ?>
          </div>
          <p>
            <?php $content = get_the_content();
            $trimmed_content = wp_trim_words( $content, 40, '<a href="'. get_permalink() .'"> ...Leggi altro</a>' );
            echo $trimmed_content; ?>
          </p>



        </div>
      </div>
    </article>

  		

  <?php endwhile; ?>

  	</div>
  </div>
<?php require_once 'footer.php'; ?>