<div class="row">
  <?php $page = get_page(get_the_ID()); ?>

  <div class="col-12 mt-5">
    <h1><?php echo get_the_title(); ?></h1>
  </div>
  <?php if (has_post_thumbnail()): ?>
    <div class="col-12">
      <div id="page_featured">
        <div style="background-image: url(<?php echo get_the_post_thumbnail_url()?>);"></div>
        <img src="<?php echo get_the_post_thumbnail_url()?>" alt="" class="img-fluid">
      </div>
    </div>
  <?php endif ?>
  <?php echo apply_filters('the_content', $page->post_content); ?>  
</div>