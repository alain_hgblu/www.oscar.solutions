var np = '/usr/local/lib/node_modules/';

var gulp = require('gulp');
var php = require(np+'gulp-connect-php'),
    browserSync = require(np+'browser-sync'),
    sass = require(np+'gulp-sass');

var sftp = require(np+'gulp-sftp');


var reload  = browserSync.reload;

gulp.task('sftp', function () {
    return gulp.src('./*')
        .pipe(sftp({
            host: '35.188.118.159',
            user: 'sammosna',
            key: '~/.ssh/google_compute_engine',
            remotePath: '/var/www/html/oscar.solutions/wp-content/themes/www.oscar.solutions.v2/'
        }));
});



/*
gulp.task('php', function() {
    php.server({ base: '.', port: 8010, keepalive: true});
});

gulp.task('browser-sync',['sass', 'php'], function() {
    browserSync({
        proxy: '127.0.0.1:8010',
        port: 8080,
        open: true,
        notify: false
    });
});
*/

gulp.task('sass', function () {
  return gulp.src('css/*.scss')
              .pipe(sass())
              .pipe(gulp.dest('css/'))
              .pipe(browserSync.reload({stream: true})); // prompts a reload after compilation
});

/*
gulp.task('default', ['sass', 'browser-sync'], function () {
	gulp.watch("css/*.scss", ['sass']);
  gulp.watch(['./*.php'], [reload]);
});
*/

// gulp.task('default', ['sass', 'sftp'], function () {
gulp.task('default', ['sass'], function () {
  gulp.watch("css/*.scss", ['sass']);
//   gulp.watch("./*", ['sftp']);
});
