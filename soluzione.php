<?php /* Template Name: Soluzione */ ?>

<?php require_once 'header.php'; ?>


<div id="interna" class="container">
	<div class="row">
		<div class="col-md-12 product-content">
      <?php $layout = get_field('layout'); 
      switch ($layout) {
        case '1':
          require 'soluzione.layout01.php';
          break;

        case '2':
          require 'soluzione.layout02.php';
          break;
        
        default:
          require 'soluzione.layout01.php';
          break;
      }?>
		</div>
	</div>
</div>


<?php require_once 'footer.php'; ?>